﻿using ACE.Entity;

namespace ACE.Mods.PetFollow
{
    [HarmonyPatch]
    public class PatchClass
    {
        #region Settings
        const int RETRIES = 10;

        public Settings Settings = new();
        static string settingsPath => Path.Combine(Mod.ModPath, "Settings.json");
        private FileInfo settingsInfo = new(settingsPath);

        private JsonSerializerOptions _serializeOptions = new()
        {
            WriteIndented = true,
            AllowTrailingCommas = true,
            Converters = { new JsonStringEnumConverter(JsonNamingPolicy.CamelCase) },
            Encoder = JavaScriptEncoder.UnsafeRelaxedJsonEscaping,
        };
        private static Position? _lastPos;

        private void SaveSettings()
        {
            string jsonString = JsonSerializer.Serialize(Settings, _serializeOptions);

            if (!settingsInfo.RetryWrite(jsonString, RETRIES))
            {
                ModManager.Log($"Failed to save settings to {settingsPath}...", ModManager.LogLevel.Warn);
                Mod.State = ModState.Error;
            }
        }

        private void LoadSettings()
        {
            if (!settingsInfo.Exists)
            {
                ModManager.Log($"Creating {settingsInfo}...");
                SaveSettings();
            }
            else
                ModManager.Log($"Loading settings from {settingsPath}...");

            if (!settingsInfo.RetryRead(out string jsonString, RETRIES))
            {
                Mod.State = ModState.Error;
                return;
            }

            try
            {
                Settings = JsonSerializer.Deserialize<Settings>(jsonString, _serializeOptions);
            }
            catch (Exception)
            {
                ModManager.Log($"Failed to deserialize Settings: {settingsPath}", ModManager.LogLevel.Warn);
                Mod.State = ModState.Error;
                return;
            }
        }
        #endregion

        #region Start/Shutdown
        public void Start()
        {
            //Need to decide on async use
            Mod.State = ModState.Loading;
            LoadSettings();

            if (Mod.State == ModState.Error)
            {
                ModManager.DisableModByPath(Mod.ModPath);
                return;
            }

            Mod.State = ModState.Running;
        }

        public void Shutdown()
        {
            //if (Mod.State == ModState.Running)
            // Shut down enabled mod...

            //If the mod is making changes that need to be saved use this and only manually edit settings when the patch is not active.
            //SaveSettings();

            if (Mod.State == ModState.Error)
                ModManager.Log($"Improper shutdown: {Mod.ModPath}", ModManager.LogLevel.Error);
        }
        #endregion

        #region Commands


        [CommandHandler("recallpet", AccessLevel.Player, CommandHandlerFlag.RequiresWorld)]
        public static void HandleRecallPet(Session session, params string[] parameters)
        {
            if (session.Player.CurrentActivePet is null)
            {
                session.WorldBroadcast("You don't have a pet!");
                return;
            }

            var mod = ModManager.GetModContainerByName("ACE.Mods.PathFinding");
            if (mod is null || mod.Instance is not PathFinding.Mod hostMod)
            {
                ModManager.Log($"ACE.Mods.PathFinding not found."); 
                return;
            }

            var pet = session.Player.CurrentActivePet.PhysicsObj.Position;
            var player = session.Player.PhysicsObj.Position;
            var path = hostMod.PathFinder.FindRoute(_lastPos ?? pet.ACEPosition(), player.ACEPosition());

            if (path is null || path.Count < 3)
            {
                _lastPos = null;
                session.Player.CurrentActivePet.CancelMoveTo();
                session.Player.CurrentActivePet.MoveToPosition(player.ACEPosition());
            }
            else
            {
                _lastPos = path.Skip(1).FirstOrDefault();
                session.Player.CurrentActivePet.CancelMoveTo();
                session.Player.CurrentActivePet.MoveToPosition(path.Skip(1).FirstOrDefault());
            }
        }
        #endregion

        #region Patches
        //[HarmonyPrefix]
        //[HarmonyPatch(typeof(Creature), nameof(Creature.GetDeathMessage), new Type[] { typeof(DamageHistoryInfo), typeof(DamageType), typeof(bool) })]
        //public static void PreDeathMessage(DamageHistoryInfo lastDamagerInfo, DamageType damageType, bool criticalHit, ref Creature __instance)
        //{
        //  ...
        //}
        #endregion
    }

}
